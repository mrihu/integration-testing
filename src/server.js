const express = require('express');
const converter = require('./converter');
const app = express();
const PORT = 3000;

// Welcome endpoint
app.get('/', (req,res) => res.send("Welcome!"));

// RGB to Hex endpoint
app.get('/rgb-to-hex', (req,res) => {
    const red = Number(req.query.r);
    const green = Number(req.query.g);
    const blue = Number(req.query.b);
    const hex = converter.rgbToHex(red, green, blue);
    res.send(hex);
});

// Hex to RGB endpoint
app.get('/hex-to-rgb', (req,res) => {
    const hex = req.query.h;
    const rgb = converter.hexToRgb(hex);
    res.send(rgb);
})

if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    app.listen(PORT, () => console.log("Server listening..."));
}

console.log("NODE_ENV: " + process.env.NODE_ENV);